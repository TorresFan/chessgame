﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rook : Chessman {

    public override bool[,] isPossibleToMove()
    {
        bool[,] rookRules = new bool[8, 8];
        Chessman c;
        int i;
        i = CurrentX;
        //right
        while (true)
        {
            i++;
            if (i >= 8)
                break;
            c = BoardManager.BoardManagerInstance.Chessmans[i, CurrentY];
            if (c == null)
            {
                rookRules[i, CurrentY] = true;
            }
            else
            {
                if(c.isWhite!=isWhite)
                    rookRules[i, CurrentY] = true;
                break;
            }
        }
        //left
        i = CurrentX;
        while (true)
        {
            i--;
            if (i <0)
                break;
            c = BoardManager.BoardManagerInstance.Chessmans[i, CurrentY];
            if (c == null)
            {
                rookRules[i, CurrentY] = true;
            }
            else
            {
                if (c.isWhite != isWhite)
                    rookRules[i, CurrentY] = true;
                break;
            }
        }
        //up
    
        i = CurrentY;
        while (true)
        {
            i++;
            if (i >=8)
                break;
            c = BoardManager.BoardManagerInstance.Chessmans[CurrentX, i];
            if (c == null)
            {
                rookRules[CurrentX, i] = true;
            }
            else
            {
                if (c.isWhite != isWhite)
                    rookRules[CurrentX, i] = true;
                break;
            }
        }
        //down
        i = CurrentY;
        while (true)
        {
            i--;
            if (i<0)
                break;
            c = BoardManager.BoardManagerInstance.Chessmans[CurrentX, i];
            if (c == null)
            {
                rookRules[CurrentX, i] = true;
            }
            else
            {
                if (c.isWhite != isWhite)
                    rookRules[CurrentX, i] = true;
                break;
            }
        }
        return rookRules;
    }
}
