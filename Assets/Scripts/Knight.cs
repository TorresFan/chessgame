﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knight : Chessman {

    public override bool[,] isPossibleToMove()
    {
        bool[,] KnightRules=new bool[8,8];
        
        //UPLEFT
        KnightMove(CurrentX - 1,CurrentY+2,ref KnightRules);
        //UPRight
        KnightMove(CurrentX + 1, CurrentY + 2, ref KnightRules);
        //
        KnightMove(CurrentX + 2, CurrentY + 1, ref KnightRules);
        //DOWnRight
        KnightMove(CurrentX + 2, CurrentY - 1, ref KnightRules);
        //UPLEFT
        KnightMove(CurrentX - 1, CurrentY - 2, ref KnightRules);
        //UPRight
        KnightMove(CurrentX + 1, CurrentY - 2, ref KnightRules);
        //
        KnightMove(CurrentX - 2, CurrentY + 1, ref KnightRules);
        //DOWnRight
        KnightMove(CurrentX - 2, CurrentY - 1, ref KnightRules);

        return KnightRules;
    }
    public void KnightMove(int x, int y,ref bool [,]rule)
    {
        Chessman c;
        if (x >= 0 && x < 8 && y >= 0 && y < 8)
        {
            c = BoardManager.BoardManagerInstance.Chessmans[x, y];
            if (c == null)
                rule[x, y] = true;
            else if (c.isWhite != isWhite)
                rule[x, y] = true;

        }
    }
}
