﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : Chessman {

    public override bool[,] isPossibleToMove()
    {
        bool[,] pawnRules=new bool[8,8];
        Chessman c, c2;
        //white team move
        if (isWhite)
        {
            //first move
            if (CurrentY == 1)
            {
                c = BoardManager.BoardManagerInstance.Chessmans[CurrentX, CurrentY + 1];
                c2 = BoardManager.BoardManagerInstance.Chessmans[CurrentX, CurrentY + 2];
                if (c == null && c2 == null)
                    pawnRules[CurrentX, CurrentY + 2] = true;
            }
            //all other moves
            if(CurrentY!=7)
            {
                c = BoardManager.BoardManagerInstance.Chessmans[CurrentX, CurrentY + 1];
                if (c == null)
                    pawnRules[CurrentX, CurrentY + 1] = true;
            }
                
            //attack left
            if (CurrentX != 0 && CurrentY != 7)
            {
                c = BoardManager.BoardManagerInstance.Chessmans[CurrentX - 1, CurrentY + 1];
                if (c != null&&!c.isWhite)
                {
                    pawnRules[CurrentX - 1, CurrentY + 1] = true;
                }
            }
            //attack right
            if (CurrentX != 7 && CurrentY != 7)
            {
                c = BoardManager.BoardManagerInstance.Chessmans[CurrentX + 1, CurrentY + 1];
                if (c != null && !c.isWhite)
                {
                    pawnRules[CurrentX + 1, CurrentY + 1] = true;
                }
            }
        }
        //black team
        else
        {
            //first move
            if (CurrentY == 6)
            {
                c = BoardManager.BoardManagerInstance.Chessmans[CurrentX, CurrentY - 1];
                c2 = BoardManager.BoardManagerInstance.Chessmans[CurrentX, CurrentY - 2];
                if (c == null && c2 == null)
                    pawnRules[CurrentX, CurrentY - 2] = true;
            }
            //all other moves
            if (CurrentY != 0)
            {
                c = BoardManager.BoardManagerInstance.Chessmans[CurrentX, CurrentY - 1];
                if (c == null)
                    pawnRules[CurrentX, CurrentY - 1] = true;
            }

            //attack left
            if (CurrentX != 0 && CurrentY != 0)
            {
                c = BoardManager.BoardManagerInstance.Chessmans[CurrentX - 1, CurrentY - 1];
                if (c != null && c.isWhite)
                {
                    pawnRules[CurrentX - 1, CurrentY - 1] = true;
                }
            }
            //attack right
            if (CurrentX != 7 && CurrentY != 0)
            {
                c = BoardManager.BoardManagerInstance.Chessmans[CurrentX + 1, CurrentY - 1];
                if (c != null && c.isWhite)
                {
                    pawnRules[CurrentX + 1, CurrentY - 1] = true;
                }
            }
        }

        return pawnRules;
    }

}
