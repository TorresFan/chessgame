﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class King : Chessman {

    public override bool[,] isPossibleToMove()
    {
        bool[,] KingRule = new bool[8, 8];
        Chessman c;
        int i, j;
        //top side
        i = CurrentX-1;
        j = CurrentY+1;
        if (CurrentY != 7)
        {
            for (int k = 0; k < 3; k++)
            {
                if (i >= 0 || i < 8)
                {
                    c = BoardManager.BoardManagerInstance.Chessmans[i,j];
                    if (c == null)
                        KingRule[i, j] = true;
                    else if (c.isWhite != isWhite)
                        KingRule[i, j] = true;

                }
                i++;
            }
        }
        //down side
        i = CurrentX - 1;
        j = CurrentY - 1;
        if (CurrentY != 0)
        {
            for (int k = 0; k < 3; k++)
            {
                if (i >= 0 || i < 8)
                {
                    c = BoardManager.BoardManagerInstance.Chessmans[i, j];
                    if (c == null)
                        KingRule[i, j] = true;
                    else if (c.isWhite != isWhite)
                        KingRule[i, j] = true;

                }
                i++;
            }
        }
        //Middle left
        if (CurrentX != 0)
        {
            c = BoardManager.BoardManagerInstance.Chessmans[CurrentX-1, CurrentY];
            if (c == null)
                KingRule[CurrentX - 1, CurrentY] = true;
            else if (c.isWhite != isWhite)
                KingRule[CurrentX - 1, CurrentY] = true;

        }
        //Middle right
        if (CurrentX != 7)
        {
            c = BoardManager.BoardManagerInstance.Chessmans[CurrentX + 1, CurrentY];
            if (c == null)
                KingRule[CurrentX + 1, CurrentY] = true;
            else if (c.isWhite != isWhite)
                KingRule[CurrentX + 1, CurrentY] = true;

        }
        return KingRule;
    }
}
