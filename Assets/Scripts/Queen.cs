﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Queen : Chessman {
    public override bool[,] isPossibleToMove()
    {
        bool[,] QueenRule = new bool[8, 8];
        Chessman c;
        int i, j;


        i = CurrentX;
        j = CurrentY;
        //Upright
        while (true)
        {
            i++;
            j++;

            if (i >= 8 || j >= 8)
                break;
            c = BoardManager.BoardManagerInstance.Chessmans[i, j];
            if (c == null)
            {
                QueenRule[i, j] = true;
            }
            else
            {
                if (c.isWhite != isWhite)
                    QueenRule[i, j] = true;
                break;
            }
        }
        //Upleft
        i = CurrentX;
        j = CurrentY;
        while (true)
        {
            i--;
            j++;

            if (i < 0 || j >= 8)
                break;
            c = BoardManager.BoardManagerInstance.Chessmans[i, j];
            if (c == null)
            {
                QueenRule[i, j] = true;
            }
            else
            {
                if (c.isWhite != isWhite)
                    QueenRule[i, j] = true;
                break;
            }
        }
        //DownRight
        i = CurrentX;
        j = CurrentY;
        while (true)
        {
            i++;
            j--;

            if (i >= 8 || j < 0)
                break;
            c = BoardManager.BoardManagerInstance.Chessmans[i, j];
            if (c == null)
            {
                QueenRule[i, j] = true;
            }
            else
            {
                if (c.isWhite != isWhite)
                    QueenRule[i, j] = true;
                break;
            }
        }
        //DownLeft
        i = CurrentX;
        j = CurrentY;
        while (true)
        {
            i--;
            j--;

            if (i < 0 || j < 0)
                break;
            c = BoardManager.BoardManagerInstance.Chessmans[i, j];
            if (c == null)
            {
                QueenRule[i, j] = true;
            }
            else
            {
                if (c.isWhite != isWhite)
                    QueenRule[i, j] = true;
                break;
            }
        }
        i = CurrentX;
        //right
        while (true)
        {
            i++;
            if (i >= 8)
                break;
            c = BoardManager.BoardManagerInstance.Chessmans[i, CurrentY];
            if (c == null)
            {
                QueenRule[i, CurrentY] = true;
            }
            else
            {
                if (c.isWhite != isWhite)
                    QueenRule[i, CurrentY] = true;
                break;
            }
        }
        //left
        i = CurrentX;
        while (true)
        {
            i--;
            if (i < 0)
                break;
            c = BoardManager.BoardManagerInstance.Chessmans[i, CurrentY];
            if (c == null)
            {
                QueenRule[i, CurrentY] = true;
            }
            else
            {
                if (c.isWhite != isWhite)
                    QueenRule[i, CurrentY] = true;
                break;
            }
        }
        //up

        i = CurrentY;
        while (true)
        {
            i++;
            if (i >= 8)
                break;
            c = BoardManager.BoardManagerInstance.Chessmans[CurrentX, i];
            if (c == null)
            {
                QueenRule[CurrentX, i] = true;
            }
            else
            {
                if (c.isWhite != isWhite)
                    QueenRule[CurrentX, i] = true;
                break;
            }
        }
        //down
        i = CurrentY;
        while (true)
        {
            i--;
            if (i < 0)
                break;
            c = BoardManager.BoardManagerInstance.Chessmans[CurrentX, i];
            if (c == null)
            {
                QueenRule[CurrentX, i] = true;
            }
            else
            {
                if (c.isWhite != isWhite)
                    QueenRule[CurrentX, i] = true;
                break;
            }
        }
        return QueenRule;
    }

}
