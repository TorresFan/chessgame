﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardHighlights : MonoBehaviour {

	public static BoardHighlights Instance { set; get; }
    public GameObject highLightsPrefabs;
    private List<GameObject> highLights;
    void Start()
    {
        //Debug.Log("this is the start");
        Instance = this;
        if (Instance == null)
            Debug.Log("Error");
        highLights = new List<GameObject>();

    }
    private GameObject GetHighLightsObject()
    {
        GameObject go = highLights.Find(g => !g.activeSelf);
        if (go == null)
        {
            go = Instantiate(highLightsPrefabs);
            highLights.Add(go);
        }
        return go;
    }
    public void HighLightAllowedMoves(bool[,]moves)
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (moves[i, j])
                {
                    GameObject go = GetHighLightsObject();
                    go.SetActive(true);
                    go.transform.position = new Vector3(i+0.5f, 0, j+0.5f);

                }
            }
        }
    }
    public void HideHighLights()
    {
        foreach (GameObject go in highLights)
        {
            go.SetActive(false);
        }
    }
}
