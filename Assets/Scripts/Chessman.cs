﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chessman : MonoBehaviour {

    private int currentX;
    public int CurrentX
    {
        get
        {
            return currentX;
        }

        set
        {
            currentX = value;
        }
    }
    private int currentY;
    public int CurrentY
    {
        get
        {
            return currentY;
        }

        set
        {
            currentY = value;
        }
    }
    public bool isWhite;
    public void setPosition(int x,int y)
    {
        CurrentX = x;
        CurrentY = y;
    }
    public virtual bool[,] isPossibleToMove()
    {
        return new bool[8,8];
    }

    
}
