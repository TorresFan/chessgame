﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bishop : Chessman {
    public override bool[,] isPossibleToMove()
    {
        bool[,] BishopRule = new bool[8, 8];
        Chessman c;
        int i,j;
        i = CurrentX;
        j = CurrentY;
        //Upright
        while (true)
        {
            i++;
            j++;

            if (i >= 8||j>=8)
                break;
            c = BoardManager.BoardManagerInstance.Chessmans[i, j];
            if (c == null)
            {
                BishopRule[i, j] = true;
            }
            else
            {
                if (c.isWhite != isWhite)
                    BishopRule[i, j] = true;
                break;
            }
        }
        //Upleft
        i = CurrentX;
        j = CurrentY;
        while (true)
        {
            i--;
            j++;

            if (i <0 || j >= 8)
                break;
            c = BoardManager.BoardManagerInstance.Chessmans[i, j];
            if (c == null)
            {
                BishopRule[i, j] = true;
            }
            else
            {
                if (c.isWhite != isWhite)
                    BishopRule[i, j] = true;
                break;
            }
        }
        //DownRight
        i = CurrentX;
        j = CurrentY;
        while (true)
        {
            i++;
            j--;

            if (i >= 8 || j <0)
                break;
            c = BoardManager.BoardManagerInstance.Chessmans[i, j];
            if (c == null)
            {
                BishopRule[i, j] = true;
            }
            else
            {
                if (c.isWhite != isWhite)
                    BishopRule[i, j] = true;
                break;
            }
        }
        //DownLeft
        i = CurrentX;
        j = CurrentY;
        while (true)
        {
            i--;
            j--;

            if (i < 0 || j <0)
                break;
            c = BoardManager.BoardManagerInstance.Chessmans[i, j];
            if (c == null)
            {
                BishopRule[i, j] = true;
            }
            else
            {
                if (c.isWhite != isWhite)
                    BishopRule[i, j] = true;
                break;
            }
        }
        return BishopRule;
    }

}
